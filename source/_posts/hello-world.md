---
title: Bonjour
date: 2023-01-02 20:54:02
---

Ce site ayant été réalisé dans l'urgence, voici une brève description de la manière de le maintenir.

L'adresse de la page d'accueil pointe vers: `~/source/index.md`

L'adresse de la page de feishu pointe sur: `~/source/feishu/index.md`

L'adresse de la page de remerciement pointe vers: `~/source/thanks/index.md`

Vous pouvez le cloner localement et le modifier directement, puis le pousser, les pages Cloudflare le construiront automatiquement.

L'adresse de la page dynamique pointe vers: Documents sous `~/source/_posts/` 

Vous devez d'abord

```shell
npm install hexo-cil
```

Ensuite, à la racine du projet, exécutez

```shell
npx hexo new "title"
```

Créer un nouveau `title.md`

Note : Le fichier créé contient plusieurs lignes de tags au début par défaut, il suffit de supprimer la ligne de "tags".
