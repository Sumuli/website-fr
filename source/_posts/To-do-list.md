---
title: Liste des tâches
date: 2023-01-04 15:21:27
---

1. Attirer davantage de personnes intéressées à nous rejoindre
2. Préparation du nouveau forum
3. Restaurer le référentiel du projet
4. Lancement d'une branche basée sur Arch Linux sur une version principale basée sur Debian
5. Résoudre les problèmes dans le processus de construction du système
6. Ajouter de nouveaux moyens de faire de la publicité
