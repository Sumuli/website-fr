---
title: Liste de remerciements
date: 2023-01-02 20:53:32
---

### CutefishOS

- Les mises à jour ont maintenant été abandonnées.
- Merci pour sa contribution à la communauté open source.
- Sa beauté et sa simplicité me rendent accro.
- Le suffixe `.cf` du nom de domaine de ce site web lui rend hommage.
- CutefishOS Team

---

### Chers développeurs

- Ils viennent de toutes les régions du monde.
- Ils n'ont jamais perdu de vue l'intention initiale.
- Ils ne veulent pas être payés, ils aiment l'open source et ils aiment Linux.
- Piscesys Association
- Je souhaite à chaque développeur une vie heureuse!
---

### Hexo&Next

- Leur existence est à la base de l'existence de ce site web.
- Il a une approche facile à utiliser (construit à partir de Markdown).
- J'espère que de plus en plus de personnes l'utiliseront.
- Hexo Team & Next Team

---

### Freenom

- Il fournit le nom de domaine de ce site web.
- C'est gratuit et il n'y a pas de barrière à l'utilisation.
- Bien qu'il ne soit pas utilisé pendant longtemps.
- Mais cela rend ce site accessible.
- Qu'il devienne vraiment "un nom que tout le monde connaît".
- Freenom

---

### Cloudflare Pages

- Il héberge ce site.
- Gratuit et sans limite de nombre d'utilisations.
- Rapide en Chine.
- Cloudflare Pages

---

### Liste des contributeurs

|         |Introduction|
|---------|:--:|
|<img src="https://gitlab.com/uploads/-/system/user/avatar/13366105/avatar.png" height=80 width=80 alt="wrefiyu" />|[wrefiyu lanka](https://gitlab.com/wrefiyu)<br />**Créateur du projet , Créateur de site web , Administrateur du forum**|
|<img src="https://gitlab.com/uploads/-/system/user/avatar/13367310/avatar.png" height=80 width=80 alt="wrefiyu" />|[Zhuang](https://gitlab.com/zhuangzhuang20080802)<br />**Créateur de sites web , Créateur du forum , Administrateur du forum**|
|<img src="https://gitlab.com/uploads/-/system/user/avatar/9567858/avatar.png" height=80 width=80 alt="wrefiyu" />|[Gavin Zhao](https://gitlab.com/GZGavinZhao)|
|<img src="https://gitlab.com/uploads/-/system/user/avatar/11697910/avatar.png" height=80 width=80 alt="wrefiyu" />|[月読時](https://gitlab.com/TsukuyomiToki)|
|<img src="https://gitlab.com/uploads/-/system/user/avatar/13339134/avatar.png" height=80 width=80 alt="wrefiyu" />|[Jack77793](https://gitlab.com/Jack77793)<br />**Responsable de la version anglaise du site web , Modérateur du forum**|
